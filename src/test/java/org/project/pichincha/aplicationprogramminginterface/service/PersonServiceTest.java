package org.project.pichincha.aplicationprogramminginterface.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.project.pichincha.aplicationprogramminginterface.entity.GenderEnum;
import org.project.pichincha.aplicationprogramminginterface.entity.Person;
import org.project.pichincha.aplicationprogramminginterface.presenter.PersonPresenter;
import org.project.pichincha.aplicationprogramminginterface.repository.PersonRepository;
import org.project.pichincha.aplicationprogramminginterface.service.impl.PersonServiceImpl;

import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PersonServiceTest {

    @Mock
    private PersonRepository personRepository;

    @Spy
    private ModelMapper modelMapper;

    @InjectMocks
    private PersonService personService = new PersonServiceImpl();

    @Test
    void shouldSave() {
        PersonPresenter personPresenter = PersonPresenter.builder()
                .id(UUID.randomUUID()).age(25).dni("1800000001")
                .phone("032855555").address("Ambato")
                .gender(GenderEnum.MASCULINO)
                .name("Juan Jose").build();

        personService.save(personPresenter);

        ArgumentCaptor<Person> personArgumentCaptor = ArgumentCaptor.forClass(Person.class);
        verify(personRepository, times(1)).save(personArgumentCaptor.capture());
        Assertions.assertThat(personArgumentCaptor.getValue().getId()).isNotNull();
        Assertions.assertThat(personArgumentCaptor.getValue().getAge()).isEqualTo(personPresenter.getAge());
        Assertions.assertThat(personArgumentCaptor.getValue().getDni()).isEqualTo(personPresenter.getDni());
        Assertions.assertThat(personArgumentCaptor.getValue().getPhone()).isEqualTo(personPresenter.getPhone());
        Assertions.assertThat(personArgumentCaptor.getValue().getAddress()).isEqualTo(personPresenter.getAddress());
        Assertions.assertThat(personArgumentCaptor.getValue().getGender()).isEqualTo(personPresenter.getGender());
        Assertions.assertThat(personArgumentCaptor.getValue().getName()).isEqualTo(personPresenter.getName());

    }

    @Test
    void shouldDelete() {
        personService.delete(UUID.randomUUID());
        verify(personRepository, times(1)).deleteById(any());
    }
}

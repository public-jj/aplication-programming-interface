package org.project.pichincha.aplicationprogramminginterface.service;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.project.pichincha.aplicationprogramminginterface.entity.Customer;
import org.project.pichincha.aplicationprogramminginterface.entity.GenderEnum;
import org.project.pichincha.aplicationprogramminginterface.entity.Person;
import org.project.pichincha.aplicationprogramminginterface.presenter.CustomerPresenter;
import org.project.pichincha.aplicationprogramminginterface.presenter.PersonPresenter;
import org.project.pichincha.aplicationprogramminginterface.repository.CustomerRepository;
import org.project.pichincha.aplicationprogramminginterface.service.impl.CustomerServiceImpl;

import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private PersonService personService;

    @Spy
    private ModelMapper modelMapper;

    @InjectMocks
    private CustomerService customerService = new CustomerServiceImpl();

    @Test
    void shouldSave() {
        PersonPresenter personPresenter = PersonPresenter.builder()
                .id(UUID.randomUUID()).age(25).dni("1800000001")
                .address("Ambato").gender(GenderEnum.MASCULINO)
                .name("Juan Jose").phone("032822454").build();

        when(personService.save(any())).thenReturn(Person.builder()
                .id(UUID.randomUUID()).age(25).dni("1800000001")
                .address("Ambato").gender(GenderEnum.MASCULINO)
                .name("Juan Jose").phone("032822454").build());

        CustomerPresenter customerPresenter = CustomerPresenter.builder().id(UUID.randomUUID()).state(true).password("123").build();

        customerService.save(customerPresenter);

        ArgumentCaptor<Customer> customerArgumentCaptor = ArgumentCaptor.forClass(Customer.class);
        verify(customerRepository, times(1)).save(customerArgumentCaptor.capture());
        Assertions.assertThat(customerArgumentCaptor.getValue().getId()).isNotNull();
        Assertions.assertThat(customerArgumentCaptor.getValue().getPerson()).isNotNull();
        Assertions.assertThat(customerArgumentCaptor.getValue().isState()).isTrue();
        Assertions.assertThat(customerArgumentCaptor.getValue().getPassword()).isEqualTo(customerPresenter.getPassword());
    }

    @Test
    void shouldDelete() {
        customerService.delete(UUID.randomUUID());
        verify(customerRepository, times(1)).deleteById(any());
    }

}

package org.project.pichincha.aplicationprogramminginterface.service;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.project.pichincha.aplicationprogramminginterface.entity.Account;
import org.project.pichincha.aplicationprogramminginterface.entity.AccountTypeEnum;
import org.project.pichincha.aplicationprogramminginterface.entity.Customer;
import org.project.pichincha.aplicationprogramminginterface.presenter.AccountPresenter;
import org.project.pichincha.aplicationprogramminginterface.presenter.CustomerPresenter;
import org.project.pichincha.aplicationprogramminginterface.repository.AccountRepository;
import org.project.pichincha.aplicationprogramminginterface.repository.CustomerRepository;
import org.project.pichincha.aplicationprogramminginterface.service.impl.AccountServiceImpl;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private CustomerRepository customerRepository;

    @Spy
    private ModelMapper modelMapper;

    @InjectMocks
    private AccountService accountService = new AccountServiceImpl();

    @Test
    void shouldSave() {
        CustomerPresenter customerPresenter = CustomerPresenter.builder().id(UUID.randomUUID()).state(true).password("123").build();
        AccountPresenter accountPresenter = AccountPresenter.builder().customerPresenter(customerPresenter).accountNumber("12345").state(true).type(AccountTypeEnum.AHORROS).initialBalance(BigDecimal.TEN).build();

        when(customerRepository.findById(any())).thenReturn(Optional.of(Customer.builder().id(UUID.randomUUID()).state(true).build()));

        accountService.save(accountPresenter);

        ArgumentCaptor<Account> accountArgumentCaptor = ArgumentCaptor.forClass(Account.class);
        verify(accountRepository, times(1)).save(accountArgumentCaptor.capture());
        Assertions.assertThat(accountArgumentCaptor.getValue().getAccountNumber()).isEqualTo(accountPresenter.getAccountNumber());
        Assertions.assertThat(accountArgumentCaptor.getValue().isState()).isTrue();
        Assertions.assertThat(accountArgumentCaptor.getValue().getInitialBalance()).isEqualTo(accountPresenter.getInitialBalance());
    }
}

package org.project.pichincha.aplicationprogramminginterface.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.project.pichincha.aplicationprogramminginterface.presenter.CustomerPresenter;
import org.project.pichincha.aplicationprogramminginterface.presenter.PersonPresenter;
import org.project.pichincha.aplicationprogramminginterface.service.CustomerService;

import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTest {

    @Mock
    private CustomerService customerService;

    @InjectMocks
    private CustomerController customerController;

    @Test
    void shoulSave() {
        CustomerPresenter customerPresenter = CustomerPresenter.builder().state(true).password("123456").personPresenter(PersonPresenter.builder().build()).build();

        customerController.saveCustomer(customerPresenter);

        verify(customerService, times(1)).save(any());

    }

    @Test
    void shouldDelete() {
        customerController.deleteCustomer(UUID.randomUUID());
        verify(customerService, times(1)).delete(any());
    }

}

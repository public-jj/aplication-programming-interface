package org.project.pichincha.aplicationprogramminginterface.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.project.pichincha.aplicationprogramminginterface.entity.MovementTypeEnum;
import org.project.pichincha.aplicationprogramminginterface.presenter.AccountPresenter;
import org.project.pichincha.aplicationprogramminginterface.presenter.MovementPresenter;
import org.project.pichincha.aplicationprogramminginterface.service.MovementService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MovementControllerTest {

    @Mock
    private MovementService movementService;

    @InjectMocks
    private MovementController movementController;

    @Test
    void shouldSave() throws Exception {
        MovementPresenter movementPresenter = MovementPresenter.builder().accountPresenter(AccountPresenter.builder().build()).movementDate(new Date()).value(BigDecimal.TEN).type(MovementTypeEnum.CREDITO).build();
        movementController.saveMovement(movementPresenter);
        verify(movementService, times(1)).save(any());
    }

    @Test
    void shouldDelete() {
        movementController.deleteMovements(UUID.randomUUID());
        verify(movementService, times(1)).delete(any());
    }
}

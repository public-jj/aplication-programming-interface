package org.project.pichincha.aplicationprogramminginterface.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.project.pichincha.aplicationprogramminginterface.entity.AccountTypeEnum;
import org.project.pichincha.aplicationprogramminginterface.presenter.AccountPresenter;
import org.project.pichincha.aplicationprogramminginterface.presenter.CustomerPresenter;
import org.project.pichincha.aplicationprogramminginterface.service.AccountService;

import java.math.BigDecimal;
import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AccountControllerTest {

    @Mock
    private AccountService accountService;

    @InjectMocks
    private AccountController accountController;

    @Test
    void shouldSave() throws Exception {
        AccountPresenter accountPresenter = AccountPresenter.builder().accountNumber("123456").type(AccountTypeEnum.AHORROS).state(true).customerPresenter(CustomerPresenter.builder().build()).initialBalance(BigDecimal.TEN).build();

        accountController.saveAccount(accountPresenter);

        verify(accountService, times(1)).save(any());
    }

    @Test
    void shouldDelete() {
        accountController.deleteAccount(UUID.randomUUID());

        verify(accountService, times(1)).delete(any());
    }
}

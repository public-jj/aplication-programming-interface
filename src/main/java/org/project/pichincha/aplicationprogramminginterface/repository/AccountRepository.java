package org.project.pichincha.aplicationprogramminginterface.repository;

import org.project.pichincha.aplicationprogramminginterface.entity.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AccountRepository extends CrudRepository<Account, UUID> {

    Account findByAccountNumber(String accountNumber);
}

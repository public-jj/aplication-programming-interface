package org.project.pichincha.aplicationprogramminginterface.repository;

import org.project.pichincha.aplicationprogramminginterface.entity.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PersonRepository extends CrudRepository<Person, UUID> {
}

package org.project.pichincha.aplicationprogramminginterface.repository;

import org.project.pichincha.aplicationprogramminginterface.entity.Account;
import org.project.pichincha.aplicationprogramminginterface.entity.Movement;
import org.project.pichincha.aplicationprogramminginterface.entity.MovementTypeEnum;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Repository
public interface MovementRepository extends CrudRepository<Movement, UUID> {
    Movement findFirstByAccountOrderByMovementDateDesc(Account account);

    @Query("select coalesce(sum(value), 0) from Movement m where m.type = :movementTypeEnum and m.account = :account and m.movementDate = :today")
    BigDecimal getTotalDailyDebitByAccount(@Param("account") Account account, @Param("movementTypeEnum") MovementTypeEnum movementTypeEnum, @Param("today") Date today);

    List<Movement> findByMovementDateAndAccount(Date movementDate, Account account);

}

package org.project.pichincha.aplicationprogramminginterface.repository;

import org.project.pichincha.aplicationprogramminginterface.entity.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, UUID> {
}

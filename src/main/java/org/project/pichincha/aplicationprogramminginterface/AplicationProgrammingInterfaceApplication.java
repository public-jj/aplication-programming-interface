package org.project.pichincha.aplicationprogramminginterface;

import org.apache.commons.io.IOUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class AplicationProgrammingInterfaceApplication {

    private static final String DB_PASSWORD_FILE_ENV_NAME = "DB_PASSWORD_FILE";

    private static final String DEBIT_DAILY_LIMIT_FILE_ENV_NAME = "DEBIT_DAILY_LIMIT_FILE";

    public static void main(String[] args) throws IOException {
        Set<String> arguments = new HashSet<>(Arrays.asList(args));

        if(System.getenv().get(DB_PASSWORD_FILE_ENV_NAME) != null){
            arguments.add("--spring.datasource.password="+ readPasswordFromFile(System.getenv().get(DB_PASSWORD_FILE_ENV_NAME)));
        }

        if(System.getenv().get(DEBIT_DAILY_LIMIT_FILE_ENV_NAME) != null) {
            arguments.add("--app.parameter.daily-debit-limit="+ readPasswordFromFile(System.getenv().get(DB_PASSWORD_FILE_ENV_NAME)));
        }

        args = arguments.toArray(new String[]{});

        SpringApplication.run(AplicationProgrammingInterfaceApplication.class, args);
    }

    private static String readPasswordFromFile(String pathFile) throws IOException {
        return IOUtils.toString(new FileReader(pathFile)).replace("\n", "");
    }
}

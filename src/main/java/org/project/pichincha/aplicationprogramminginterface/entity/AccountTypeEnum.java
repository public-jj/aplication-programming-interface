package org.project.pichincha.aplicationprogramminginterface.entity;

public enum AccountTypeEnum {

    AHORROS, CORRIENTE
}

package org.project.pichincha.aplicationprogramminginterface.entity;

public enum GenderEnum {
    MASCULINO, FEMENINO
}

package org.project.pichincha.aplicationprogramminginterface.entity;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
@ToString(of = "id")
@Table(name = "persons")
@Entity
public class Person {

    @Id
    @GeneratedValue
    private UUID id;


    private String name;

    @Enumerated(EnumType.STRING)
    private GenderEnum gender;

    private Integer age;

    private String dni;

    private String address;

    private String phone;

}

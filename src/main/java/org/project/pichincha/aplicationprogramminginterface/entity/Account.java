package org.project.pichincha.aplicationprogramminginterface.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
@ToString(of = "id")
@Table(name = "accounts")
@Entity
public class Account {

    @Id
    @GeneratedValue
    private UUID id;

    private String accountNumber;

    @Enumerated(EnumType.STRING)
    private AccountTypeEnum type;

    private BigDecimal initialBalance;

    private boolean state;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

}

package org.project.pichincha.aplicationprogramminginterface.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
@ToString(of = "id")
@Table(name = "movements")
@Entity
public class Movement {

    @Id
    @GeneratedValue
    private UUID id;

    private Date movementDate;

    @Enumerated(EnumType.STRING)
    private MovementTypeEnum type;

    private BigDecimal value;

    private BigDecimal balance;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

}

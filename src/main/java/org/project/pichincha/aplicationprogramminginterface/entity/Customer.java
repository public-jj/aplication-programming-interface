package org.project.pichincha.aplicationprogramminginterface.entity;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
@ToString(of = "id")
@Table(name = "customers")
@Entity
public class Customer {

    @Id
    @GeneratedValue
    private UUID id;

    private String password;

    private boolean state;

    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;

}

package org.project.pichincha.aplicationprogramminginterface.controller;

import org.project.pichincha.aplicationprogramminginterface.presenter.CustomerPresenter;
import org.project.pichincha.aplicationprogramminginterface.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping("/clientes")
    public void saveCustomer(@RequestBody CustomerPresenter customerPresenter) {
        customerService.save(customerPresenter);
    }

    @DeleteMapping("/clientes")
    public void deleteCustomer(@RequestParam UUID id) {
        customerService.delete(id);
    }
}

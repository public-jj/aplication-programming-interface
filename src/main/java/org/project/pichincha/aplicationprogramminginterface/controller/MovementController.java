package org.project.pichincha.aplicationprogramminginterface.controller;

import org.project.pichincha.aplicationprogramminginterface.presenter.MovementPresenter;
import org.project.pichincha.aplicationprogramminginterface.service.MovementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class MovementController {

    @Autowired
    private MovementService movementService;

    @PostMapping("/movimientos")
    public void saveMovement(@RequestBody MovementPresenter movementPresenter) throws Exception {
        movementService.save(movementPresenter);
    }

    @DeleteMapping("/movimientos")
    public void deleteMovements(@RequestParam UUID id) {
        movementService.delete(id);
    }

    @GetMapping("/movimientos")
    public List<MovementPresenter> getMovements(@RequestParam String dateMovement, @RequestParam String accountNumber) {
        return movementService.getMovements(dateMovement, accountNumber);
    }

}

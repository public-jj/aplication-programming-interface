package org.project.pichincha.aplicationprogramminginterface.controller;

import org.project.pichincha.aplicationprogramminginterface.presenter.AccountPresenter;
import org.project.pichincha.aplicationprogramminginterface.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping("/cuentas")
    public void saveAccount(@RequestBody AccountPresenter accountPresenter) throws Exception {
        accountService.save(accountPresenter);
    }

    @DeleteMapping("/cuentas")
    public void deleteAccount(@RequestParam UUID id) {
        accountService.delete(id);
    }
}

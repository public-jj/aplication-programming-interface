package org.project.pichincha.aplicationprogramminginterface.service.impl;

import org.modelmapper.ModelMapper;
import org.project.pichincha.aplicationprogramminginterface.entity.Customer;
import org.project.pichincha.aplicationprogramminginterface.entity.Person;
import org.project.pichincha.aplicationprogramminginterface.presenter.CustomerPresenter;
import org.project.pichincha.aplicationprogramminginterface.repository.CustomerRepository;
import org.project.pichincha.aplicationprogramminginterface.service.CustomerService;
import org.project.pichincha.aplicationprogramminginterface.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private PersonService personService;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void save(CustomerPresenter customerPresenter) {
        Person person = personService.save(customerPresenter.getPersonPresenter());
        Customer customer = modelMapper.map(customerPresenter, Customer.class);
        customer.setPerson(person);
        customerRepository.save(customer);
    }

    @Override
    public void delete(UUID id) {
        customerRepository.deleteById(id);
    }
}

package org.project.pichincha.aplicationprogramminginterface.service.impl;

import org.modelmapper.ModelMapper;
import org.project.pichincha.aplicationprogramminginterface.entity.Person;
import org.project.pichincha.aplicationprogramminginterface.presenter.PersonPresenter;
import org.project.pichincha.aplicationprogramminginterface.repository.PersonRepository;
import org.project.pichincha.aplicationprogramminginterface.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Person save(PersonPresenter personPresenter) {
        Person person = modelMapper.map(personPresenter, Person.class);
        personRepository.save(person);
        return person;
    }

    @Override
    public void delete(UUID id) {
        personRepository.deleteById(id);
    }
}

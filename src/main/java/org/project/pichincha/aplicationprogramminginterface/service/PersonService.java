package org.project.pichincha.aplicationprogramminginterface.service;

import org.project.pichincha.aplicationprogramminginterface.entity.Person;
import org.project.pichincha.aplicationprogramminginterface.presenter.PersonPresenter;

import java.util.UUID;

public interface PersonService {

    Person save(PersonPresenter personPresenter);

    void delete(UUID id);
}

package org.project.pichincha.aplicationprogramminginterface.service.impl;

import org.modelmapper.ModelMapper;
import org.project.pichincha.aplicationprogramminginterface.entity.Account;
import org.project.pichincha.aplicationprogramminginterface.entity.Customer;
import org.project.pichincha.aplicationprogramminginterface.presenter.AccountPresenter;
import org.project.pichincha.aplicationprogramminginterface.repository.AccountRepository;
import org.project.pichincha.aplicationprogramminginterface.repository.CustomerRepository;
import org.project.pichincha.aplicationprogramminginterface.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void save(AccountPresenter accountPresenter) {
        Customer customer = customerRepository.findById(accountPresenter.getCustomerPresenter().getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "Cliente no existe"));
        Account account = modelMapper.map(accountPresenter, Account.class);
        account.setCustomer(customer);
        accountRepository.save(account);
    }

    @Override
    public void delete(UUID id) {
        accountRepository.deleteById(id);
    }
}

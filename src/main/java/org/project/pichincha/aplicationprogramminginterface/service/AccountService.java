package org.project.pichincha.aplicationprogramminginterface.service;

import org.project.pichincha.aplicationprogramminginterface.presenter.AccountPresenter;

import java.util.UUID;

public interface AccountService {

    void save(AccountPresenter accountPresenter) throws Exception;

    void delete(UUID id);
}

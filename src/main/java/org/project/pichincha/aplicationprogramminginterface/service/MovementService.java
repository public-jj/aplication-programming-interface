package org.project.pichincha.aplicationprogramminginterface.service;

import org.project.pichincha.aplicationprogramminginterface.presenter.MovementPresenter;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface MovementService {

    void save(MovementPresenter movementPresenter) throws Exception;

    void delete(UUID id);

    List<MovementPresenter> getMovements(String dateMovement, String accountNumber);

}

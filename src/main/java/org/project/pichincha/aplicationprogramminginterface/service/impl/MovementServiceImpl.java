package org.project.pichincha.aplicationprogramminginterface.service.impl;

import org.modelmapper.ModelMapper;
import org.project.pichincha.aplicationprogramminginterface.entity.Account;
import org.project.pichincha.aplicationprogramminginterface.entity.Movement;
import org.project.pichincha.aplicationprogramminginterface.entity.MovementTypeEnum;
import org.project.pichincha.aplicationprogramminginterface.presenter.AccountPresenter;
import org.project.pichincha.aplicationprogramminginterface.presenter.CustomerPresenter;
import org.project.pichincha.aplicationprogramminginterface.presenter.MovementPresenter;
import org.project.pichincha.aplicationprogramminginterface.presenter.PersonPresenter;
import org.project.pichincha.aplicationprogramminginterface.repository.AccountRepository;
import org.project.pichincha.aplicationprogramminginterface.repository.MovementRepository;
import org.project.pichincha.aplicationprogramminginterface.service.MovementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MovementServiceImpl implements MovementService {

    @Autowired
    private MovementRepository movementRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Value("${app.parameter.daily-debit-limit}")
    private String debitDailyLimit;

    @Override
    public void save(MovementPresenter movementPresenter) throws Exception {
        BigDecimal dailyLimit = BigDecimal.valueOf(Double.valueOf(debitDailyLimit));
        Account account = accountRepository.findByAccountNumber(movementPresenter.getAccountPresenter().getAccountNumber());
        Movement lastMovement = movementRepository.findFirstByAccountOrderByMovementDateDesc(account);

        BigDecimal saldo = lastMovement != null ? lastMovement.getBalance() : account.getInitialBalance();
        if(movementPresenter.getType().equals(MovementTypeEnum.DEBITO) && saldo.compareTo(movementPresenter.getValue()) < 0) {
            new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "Saldo no disponible");
        }

        BigDecimal dailyAmount = movementRepository.getTotalDailyDebitByAccount(account, MovementTypeEnum.DEBITO, new Date());
        if(movementPresenter.getType().equals(MovementTypeEnum.DEBITO) && movementPresenter.getValue().add(dailyAmount == null ? BigDecimal.ZERO : dailyAmount).compareTo(dailyLimit) > 0) {
            new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "Cupo diario Excedido");
        }

        saldo = saldo.add(movementPresenter.getType().equals(MovementTypeEnum.CREDITO) ? movementPresenter.getValue() : (movementPresenter.getValue().multiply(BigDecimal.valueOf(-1))));

        Movement movement = modelMapper.map(movementPresenter, Movement.class);
        movement.setAccount(account);
        movement.setBalance(saldo);
        movementRepository.save(movement);
    }

    @Override
    public void delete(UUID id) {
        movementRepository.deleteById(id);
    }

    @Override
    public List<MovementPresenter> getMovements(String dateMovement, String accountNumber) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);

        Date date = null;
        try {
            date = formatter.parse(dateMovement);
        } catch (ParseException e) {
            new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "Error al convertir fecha");
        }
        Account account = accountRepository.findByAccountNumber(accountNumber);
        List<Movement> movements = movementRepository.findByMovementDateAndAccount(date, account);
        return movements.stream().map(this::convertToPresenter).collect(Collectors.toList());
    }

    private MovementPresenter convertToPresenter(Movement movement) {
        PersonPresenter personPresenter = modelMapper.map(movement.getAccount().getCustomer().getPerson(), PersonPresenter.class);
        CustomerPresenter customerPresenter = modelMapper.map(movement.getAccount().getCustomer(), CustomerPresenter.class);
        customerPresenter.setPersonPresenter(personPresenter);
        AccountPresenter accountPresenter = modelMapper.map(movement.getAccount(), AccountPresenter.class);
        accountPresenter.setCustomerPresenter(customerPresenter);
        MovementPresenter movementPresenter = modelMapper.map(movement, MovementPresenter.class);
        movementPresenter.setAccountPresenter(accountPresenter);

        return movementPresenter;
    }
}

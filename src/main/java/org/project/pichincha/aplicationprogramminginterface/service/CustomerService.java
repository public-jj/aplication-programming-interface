package org.project.pichincha.aplicationprogramminginterface.service;

import org.project.pichincha.aplicationprogramminginterface.presenter.CustomerPresenter;

import java.util.UUID;

public interface CustomerService {

    void save(CustomerPresenter customerPresenter);

    void delete(UUID id);
}

package org.project.pichincha.aplicationprogramminginterface.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.project.pichincha.aplicationprogramminginterface.entity.AccountTypeEnum;
import org.project.pichincha.aplicationprogramminginterface.entity.MovementTypeEnum;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MovementPresenter {

    private UUID id;

    private Date movementDate;

    private MovementTypeEnum type;

    private BigDecimal value;

    private BigDecimal balance;

    private AccountPresenter accountPresenter;
}

package org.project.pichincha.aplicationprogramminginterface.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CustomerPresenter {

    private UUID id;

    private String password;

    private boolean state;

    private PersonPresenter personPresenter;
}

package org.project.pichincha.aplicationprogramminginterface.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.project.pichincha.aplicationprogramminginterface.entity.GenderEnum;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PersonPresenter {

    private UUID id;

    private String name;

    @Enumerated(EnumType.STRING)
    private GenderEnum gender;

    private Integer age;

    private String dni;

    private String address;

    private String phone;
}

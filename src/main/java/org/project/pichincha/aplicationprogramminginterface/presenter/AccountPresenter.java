package org.project.pichincha.aplicationprogramminginterface.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.project.pichincha.aplicationprogramminginterface.entity.AccountTypeEnum;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountPresenter {

    private UUID id;

    private String accountNumber;

    private AccountTypeEnum type;

    private BigDecimal initialBalance;

    private boolean state;

    private CustomerPresenter customerPresenter;
}
